#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>

void init_field(int size, char field[][size], char alive, char dead) {
	// function to fill field with random number of alive cels
	for (int i = 0; i < size; i++)	{
		for (int j = 0; j < size; j++) {
			if (rand() % 2 == 1) {
				field[i][j] = alive;
				continue;
			}
			field[i][j] = dead;
		}
	}
}

void field_print(int size, char field[][size]) {
	// function to print field
	printf("\n\n");
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) printf("%c", field[i][j]);
		printf("\n");
	}
}

void copy_array(int size, int copy[], int og[]) {
	// copy a regular array function
	for (int i = 0; i < size; i++) copy[i] = og[i];
}

void copy_2d_array(int size, char copy[][size], char og[][size]) {
	// copy a 2d array function
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) copy[i][j] = og[i][j];
	}
}

bool in_range(int range, int offset[2]) {
	// function to check if coordinates
	// is in range of field
	if ((offset[0] < range && offset[0] >= 0)
			&& (offset[1] < range && offset[1] >= 0)){
		return true;
	}
	return false;
}

int count(int size, char field[][size], int pos[2], char alive) {
	// function to count alive neighbors
	// declaring some of variables
	char *neighbors[8];
	int alive_neighbors_count = 0;
	int coords[2];
	int neighbMatrix[][2] = {
	{-1,  1}, {0,  1}, {1,  1},
	{-1,  0}, 	   {1,  0},
	{-1, -1}, {0, -1}, {1, -1}};
	int neighbors_count = 0;
	int offset[2];

	// trying to to fill array with pointers
	// of valid neighbors
	for (int i = 0; i < 8; i++) {
		copy_array(2, offset, neighbMatrix[i]);
		coords[0] = pos[0] + offset[0]; coords[1] = pos[1] + offset[1];
		if (in_range(size, coords)) {
			neighbors[neighbors_count] = &field[coords[0]][coords[1]];
			neighbors_count++;
		}
	}

	// counting alive neighbors
	for (int i = 0; i < neighbors_count; i++) {
		if (*neighbors[i] == alive) alive_neighbors_count++;
	}

	// done
	return alive_neighbors_count;
}

void iterate(char *cell, int alive_neighbors, char dead, char alive) {
	if (alive_neighbors < 2 || alive_neighbors > 3) *cell = dead;
	else if (alive_neighbors == 3) *cell = alive;
}

int main()
{
	// setting time seed of
	// random number generation
	srand(time(NULL));

	// declaring some of variables
	char alive =           '@';
	char dead =            '`';
	int size =              55;
	int alive_neighbors;
	float time_interval =  .04;

	// field 2d array
	char field[size][size], temp_field[size][size];

	// initializing field(s)
	init_field(size, field, alive, dead);
	copy_2d_array(size, temp_field, field);

	// main game's loop
	for (;;) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				// counting alive neighbors
				alive_neighbors = count(size, field, (int[2]){i, j}, alive);
				iterate(&(temp_field[i][j]), alive_neighbors, dead, alive);

			}
		}	

		// printing field and waiting
		usleep((int)(time_interval * 1000000));
		copy_2d_array(size, field, temp_field);
		field_print(size, field);
	}
	return 0;
}
